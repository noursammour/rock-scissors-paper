express = require 'express'
passport = require 'passport'
env = 'production' or 'development'
config = require('./private/config/config')[env]

auth = require './private/config/middlewares/authorization'
mongoose = require 'mongoose'
mongoose.connect config.db

require('./private/config/passport') passport , config
app = module.exports = express()

require('./private/config/express') app, config, passport
require('./private/config/routes') app, passport, auth
port = process.env.PORT or 3000
console.log port
#
#statik = require 'statik'
#server = statik.createServer()
#server.listen process.env.PORT || 1337

app.startServer =  ->
	app.listen port, ->
		console.log "Express server listening on port %d in %s mode", port, app.settings.env