async = require 'async'
fs = require 'fs'
module.exports = (app, passport, auth) ->
	
	routeHome = require '../routes/home'	
	app.get '/', routeHome.home
	app.get "/auth/steam", passport.authenticate("steam"), (req, res) ->


	app.get "/auth/steam/callback", passport.authenticate("steam",
		failureRedirect: "/login"
	), (req, res) ->
		res.redirect "/"
	
	