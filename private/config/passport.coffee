SteamStrategy = require('passport-steam').Strategy
module.exports = (passport, config) ->

	passport.serializeUser (user, done) ->
		done null, user.id

	passport.deserializeUser (id, done) ->
		done null, id

	passport.use new SteamStrategy(
		returnURL: "http://localhost:3000/auth/steam/return"
		realm: "http://localhost:3000/"
	, (identifier, done) ->
		console.log identifier
		done null, identifier
	, (err, user) ->
		done err, user
	)