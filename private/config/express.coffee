express = require("express")
mongoStore = require("connect-mongo")(express)
flash = require("connect-flash")
helpers = require("view-helpers")


module.exports = (app, config, passport) ->
	app.set "showStackError", true

	app.use express.compress(
		filter: (req, res) ->
			/json|text|javascript|css/.test res.getHeader("Content-Type")
		level: 9
  )
	app.configure ->
		app.set "views", __dirname + '/../views'
		app.set "view engine", 'jade'
		app.use express.favicon()
		app.use express.logger("dev")
		app.use express.cookieParser()
		
		app.use express.session(
			secret: "secret"
			cookie:
				maxAge: new Date(Date.now() + (90 * 24 * 60 * 60 * 1000))
			store: new mongoStore(
				url: config.db
				collection: "sessions"
			)
		)
		
		app.use helpers(config.app.name)
		app.use express.bodyParser()
		app.use passport.initialize()
		app.use passport.session()
		app.use express.methodOverride()

		app.use flash()
		app.use app.router
		app.use express.static(__dirname + '/../../public')
		
		app.use (err, req, res, next) ->
      
			return next()  if ~err.message.indexOf("not found")
      
			console.error err.stack
			res.status(500).render "500",
				error: err.stack


    
	app.use (req, res, next) ->
		res.status(404).render "404",
			url: req.originalUrl
			error: "Not found"


