module.exports =
  development:
    root: require("path").normalize(__dirname + "/..")
    app:
      name: "Nodejs Express Mongoose Demo"

    db: "mongodb://NourSammour:passnord@linus.mongohq.com:10056/TestDB"
    facebook:
      clientID: "486077354792463"
      clientSecret: "232a7779bdcc15705b4399b064ecadcf"
      callbackURL: "http://localhost:3000/auth/facebook/callback"

    twitter:
      clientID: "CONSUMER_KEY"
      clientSecret: "CONSUMER_SECRET"
      callbackURL: "http://clinicose.herokuapp.com/auth/twitter/callback"

    github:
      clientID: "APP_ID"
      clientSecret: "APP_SECRET"
      callbackURL: "http://localhost:3000/auth/github/callback"

    google:
      clientID: "APP_ID"
      clientSecret: "APP_SECRET"
      callbackURL: "http://localhost:3000/auth/google/callback"

  test: {}
  production:
    root: require("path").normalize(__dirname + "/..")
    app:
      name: "Nodejs Express Mongoose Demo"

    db: "mongodb://NourSammour:passnord@linus.mongohq.com:10056/TestDB"
    facebook:
      clientID: "486077354792463"
      clientSecret: "232a7779bdcc15705b4399b064ecadcf"
      callbackURL: "http://localhost:3000/auth/facebook/callback"

    twitter:
      clientID: "CONSUMER_KEY"
      clientSecret: "CONSUMER_SECRET"
      callbackURL: "http://localhost:3000/auth/twitter/callback"

    github:
      clientID: "APP_ID"
      clientSecret: "APP_SECRET"
      callbackURL: "http://localhost:3000/auth/github/callback"

    google:
      clientID: "APP_ID"
      clientSecret: "APP_SECRET"
      callbackURL: "http://localhost:3000/auth/google/callback"

  	